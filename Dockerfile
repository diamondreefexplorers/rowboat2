FROM arm64v8/ros:kinetic-ros-base-xenial
MAINTAINER bigbyte sean.d.matthews@gmail.com

# install ros packages
RUN apt update && apt install -y \
    ros-kinetic-mavros \
    ros-kinetic-mavros-extras \
    ros-kinetic-joy \
    ros-kinetic-joy-teleop \
    ros-kinetic-roscpp-tutorials \
    && rm -rf /var/lib/apt/lists/*
